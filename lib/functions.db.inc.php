<?php
function carrega_link_imagem($url='#', $label='', $img1='', $img2='') {
	global $arrSETT;
	return '<a href="'.$url.'" '.( $img1 != '' && is_file($arrSETT['dir_icons_admin'].'/'.$img1) && $img2 != '' && is_file($arrSETT['dir_icons_admin'].'/'.$img2) ? 'class="rollhover_img"' : '' ).'>'.( $img1 != '' && is_file($arrSETT['dir_icons_admin'].'/'.$img1) ? '<img align="texttop" src="'.$arrSETT['url_icons_admin'].'/'.$img1.'" normal-src="'.$arrSETT['url_icons_admin'].'/'.$img1.'" '.( $img2 != '' && is_file($arrSETT['dir_icons_admin'].'/'.$img2) ? 'hover-src="'.$arrSETT['url_icons_admin'].'/'.$img2.'"' : '' ).' />' : '' ).' '.$label.'</a>';
}

function seo_friendly_url($string){
    $string = str_replace(array('[\', \']'), '', $string);
    $string = preg_replace('/\[.*\]/U', '', $string);
    $string = preg_replace('/&(amp;)?#?[a-z0-9]+;/i', '-', $string);
    $string = htmlentities($string, ENT_COMPAT, 'utf-8');
    $string = preg_replace('/&([a-z])(acute|uml|circ|grave|ring|cedil|slash|tilde|caron|lig|quot|rsquo);/i', '\\1', $string );
    $string = preg_replace(array('/[^a-z0-9]/i', '/[-]+/') , '-', $string);
    return strtolower(trim($string, '-'));
}

function trata_nome_ficheiro($filename, $extension, $key_field, $modulo) {
	global $arrSETT;
	$arr = array();
	
	$id = '';
	$arrKey_field = explode(',',$key_field);
	foreach($arrKey_field as $k=>$v) {
		$v = explode('=',$v);
		$id .= $v[1].'-';
	}
	$id = substr($id, 0, strlen($id)-1);
	
	if(!is_dir($arrSETT['dir_fotos'].'/'.$modulo)) {
		mkdir($arrSETT['dir_fotos'].'/'.$modulo);
	}
	$arr['file'] = $id.'-'.seo_friendly_url($filename).'-'.uniqid().'.'.$extension;
	$arr['pathfile'] = $arrSETT['dir_fotos'].'/'.$modulo.'/'.$arr['file'];
	
	return $arr;
}

function carrega_campo($arr, $k_arr_campos, $v_arr_campos, $inserir = 1) {
	global $arrSETT;

	$valor 		= ( isset($arr[$v_arr_campos]['editar_obrigatorio']) && $arr[$v_arr_campos]['editar_obrigatorio'] == 0 ? '' : ( $inserir == 1 ? '' : $arr[$v_arr_campos]['developer_valor']) );
	$disable 	= ( isset($arr[$v_arr_campos]['editar_proibido']) && $arr[$v_arr_campos]['editar_proibido'] == 1 && $inserir == 0 ? 'disabled="disabled"' : '' );
	$tamanho 	= ( isset($arr[$v_arr_campos]['tamanho']) && $arr[$v_arr_campos]['tamanho'] == '' ? 'maxlength="'.$arr[$v_arr_campos]['tamanho'].'"' : '' );	

	if($disable != '') {
		echo '<input type="hidden" name="'.$v_arr_campos.'" value="'.$valor.'" />';
	}
	
	switch($arr[$v_arr_campos]['tipo']) {
		case 'hidden': 
			echo '<input type="hidden" name="'.$v_arr_campos.'" value="'.$valor.'" />';
			break;

		case 'file': 
			echo '<input type="file" name="'.$v_arr_campos.'" value="'.$valor.'" />';
			break;

		case 'text': 
			echo '<input type="text" name="'.$v_arr_campos.'" value="'.$valor.'" '.$tamanho.' '.$disable.' />';
			break;

		case 'data': 
			echo '<input type="date" name="'.$v_arr_campos.'" value="'.$valor.'" '.$tamanho.' '.$disable.' />';
			break;

		case 'cor': 
			echo '<input class="jscolor" type="text" name="'.$v_arr_campos.'" value="'.$valor.'" '.$tamanho.' '.$disable.' />';
			break;

		case 'textarea': 
			echo '<textarea cols="60" rows="5" '.$disable.' name="'.$v_arr_campos.'">'.$valor.'</textarea>';
			break;
            
        case 'ckeditor':
            echo '<textarea id="'.$v_arr_campos.'" cols="60" rows="5" '.$disable.' name="'.$v_arr_campos.'">'.$valor.'</textarea>';
			echo '<script>CKEDITOR.replace("'.$v_arr_campos.'");</script>';
			break;

		case 'password': 
			echo '<input type="password" name="'.$v_arr_campos.'" value="'.$valor.'" '.$tamanho.' '.$disable.' />';
			break;

		case 'radio':
			foreach($arr[$v_arr_campos]['opcoes'] as $k=>$v) {
				echo '<input type="radio" name="'.$v_arr_campos.'" value="'.$k.'" '.( ( $inserir && $arr[$v_arr_campos]['default'] == $k ) || $valor == $k ? 'checked="checked"' : '' ).' '.$disable.' />'.$v;
			}
			break;

		case 'checkbox': 
			$arrCheckbox = explode(',', $valor);
			foreach($arr[$v_arr_campos]['opcoes'] as $k=>$v) {
				echo '<input type="checkbox" name="'.$v_arr_campos.'[]" value="'.$k.'" '.( ( $inserir && $arr[$v_arr_campos]['default'] == $k ) || in_array($k, $arrCheckbox) ? 'checked="checked"' : '' ).' '.$disable.' />'.$v;
			}
			break;
	}
}

function db_mostra_tabela($arr) {
	global $arrSETT;
	
	// ---------------------------------------	
	// determinar os campos a apresentar e qual a ordem de apresentação	
	// é obrigatório definir a variável "listagem_ordem" no array
	// não pode existir dois números de ordem iguais
	// ---------------------------------------
	$arr_campos = array();
	$arr_campos_chave = array();
	foreach($arr as $k=>$v) {
		if(isset($v['listagem']) && $v['listagem'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos[$v['listagem_ordem']] = $k;
		}
		if(isset($v['chave']) && $v['chave'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos_chave[] = $k;
		}
	}
	ksort ($arr_campos);
	
	// ---------------------------------------	
	// no SELECT podemos utilizar apenas os campos que serão necessários na listagem, 
	// mas nesse caso vamos ter a necessidade (sempre) de incluir os campos chave
	// ---------------------------------------
	$query = "SELECT ".$arr['tabela']['listagem_campos']." FROM ".$arr['tabela']['tabela_nome'];
	$res = db_query($query);
	

	if(isset($arr['inserir']['listagem']) && $arr['inserir']['listagem'] == 1) {
		echo '<div class="db_inserir"><a href="index.php?task=insert'.'&tabela='.$arr['tabela']['tabela_nome'].'">'.( isset($arr['inserir']['icon']) && is_file($arrSETT['dir_site_admin'].'/imgs/icons/'.$arr['inserir']['icon']) ? '<img align="texttop" src="'.$arrSETT['url_site_admin'].'/imgs/icons/'.$arr['inserir']['icon'].'" />' : '' ).' <i class="fa fa-user-plus"></i> '.$arr['inserir']['label'].'</a></div>';
	}
		
	// ---------------------------------------	
	// criar o cabeçalho da tabela
	// ---------------------------------------
	echo '<table>';
	echo '<tr>';
	foreach($arr_campos as $k_arr_campos=>$v_arr_campos) {
		if(isset($arr[$v_arr_campos]['listagem']) && $arr[$v_arr_campos]['listagem'] == 1) {
			echo '<th>'.$arr[$v_arr_campos]['label'].'</th>';
		}
	}
	if(isset($arr['editar']['listagem']) && $arr['editar']['listagem'] == 1) {
		echo '<th>'.$arr['editar']['label'].'</th>';
	}
	if(isset($arr['eliminar']['listagem']) && $arr['eliminar']['listagem'] == 1) {
		echo '<th>'.$arr['eliminar']['label'].'</th>';
	}
	echo '</tr>';

	// ---------------------------------------	
	// criar as linhas da tabela, de acordo com os resultados obtidos no SELECT
	// ---------------------------------------
	foreach($res as $k_res=>$v_res) {
		echo '<tr>';
		foreach($arr_campos as $k_arr_campos=>$v_arr_campos) {
			echo '<td>'.$v_res[$v_arr_campos].'</td>';
		}
		
		// ---------------------------------------	
		// preparar os campos chave para enviar por $_GET
		// ---------------------------------------	
		$strCamposChave = '';
		foreach($arr_campos_chave as $k=>$v) {
			$strCamposChave .= '&'.$v.'='.$v_res[$v];
		}
		// ---------------------------------------	
		
		if(isset($arr['editar']['listagem']) && $arr['editar']['listagem'] == 1) {
			echo '<td><a href="index.php?task=edit'.$strCamposChave.'&tabela='.$arr['tabela']['tabela_nome'].'">'.( isset($arr['editar']['icon']) && is_file($arrSETT['dir_site_admin'].'/imgs/icons/'.$arr['editar']['icon']) ? '<img align="texttop" src="'.$arrSETT['url_site_admin'].'/imgs/icons/'.$arr['editar']['icon'].'" />' : '' ).' <i class="fa fa-pencil"></i> '.$arr['editar']['label'].'</a></td>';
		}
		if(isset($arr['eliminar']['listagem']) && $arr['eliminar']['listagem'] == 1) {
			echo '<td><a href="index.php?task=delete'.$strCamposChave.'&tabela='.$arr['tabela']['tabela_nome'].'" onclick="return confirm('.'\'Are you sure?\''.');">'.( isset($arr['eliminar']['icon']) && is_file($arrSETT['dir_site_admin'].'/imgs/icons/'.$arr['eliminar']['icon']) ? '<img align="texttop" src="'.$arrSETT['url_site_admin'].'/imgs/icons/'.$arr['eliminar']['icon'].'" />' : '' ).' <i class="fa fa-trash"></i> '.$arr['eliminar']['label'].'</a></td>';
		}
		echo '</tr>';
	}
	
	// ---------------------------------------	
	// fechar a tabela
	// ---------------------------------------
	echo '</table>';
}

function db_insert_form($arr) {
    global $arrSETT;
	
	// ---------------------------------------	
	// determinar os campos a apresentar e qual a ordem de apresentação	
	// é obrigatório definir a variável "inserir_ordem" no array
	// não pode existir dois números de ordem iguais
	// ---------------------------------------
	$arr_campos = array();
	$arr_campos_chave = array();
	foreach($arr as $k=>$v) {
		if(isset($v['inserir']) && $v['inserir'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos[$v['inserir_ordem']] = $k;
		}
		if(isset($v['chave']) && $v['chave'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos_chave[] = $k;
		}
	}
	ksort($arr_campos);
	
	// ---------------------------------------	
	// no SELECT podemos utilizar apenas os campos que serão necessários na listagem, 
	// mas nesse caso vamos ter a necessidade (sempre) de incluir os campos chave
	// ---------------------------------------
	$query = "SELECT ".$arr['tabela']['listagem_campos']." FROM ".$arr['tabela']['tabela_nome'];
	$res = db_query($query);
    
	// ---------------------------------------	
	// criar o cabeçalho da tabela
	// ---------------------------------------
	echo '<table class="db_inserir">';
	echo '<form name="frmInserir" method="post" action="'.$_SERVER['PHP_SELF'].'?task=do_insert" enctype="multipart/form-data">';
	foreach($arr_campos as $k_arr_campos=>$v_arr_campos) {
		if(isset($arr[$v_arr_campos]['inserir']) && $arr[$v_arr_campos]['inserir'] == 1) {
			echo '<tr>';
			echo '<td class="label">'.$arr[$v_arr_campos]['label'].':</td>';
			echo '<td>';
			echo carrega_campo($arr, $k_arr_campos, $v_arr_campos, 1);
			echo '</td>';
			echo '</tr>';
		}
	}
	echo '<tr>';
	echo '<td></td>';
	echo '<td><input type="submit" name="submit" value="Insert" /></td>';
	echo '</tr>';
	

	// ---------------------------------------	
	// fechar a tabela
	// ---------------------------------------
	echo '</form>';
	echo '</table>';

}

function db_do_insert_form($arr) {
	global $arrSETT;
	
	// ---------------------------------------	
	// determinar os campos para inserção
	// ---------------------------------------
	$arr_campos = array();
	$arr_campos_chave = array();
	foreach($arr as $k=>$v) {
		if(isset($v['inserir']) && $v['inserir'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos[$v['inserir_ordem']] = $k;
		}
		if(isset($v['chave']) && $v['chave'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos_chave[] = $k;
		}
	}
	ksort($arr_campos);

	// ---------------------------------------	
	// no INSERT vamos detetar registo duplicados em função da informação "inserir_unico"
	// ---------------------------------------
	// construir a query_1 e query_2, partes integrantes da instrução SELECT
	// ---------------------------------------
	$query_1 = '';
	$query_2 = '';
	
	foreach($arr as $k=>$v) {
		if(isset($v['inserir_unico']) && $v['inserir_unico'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$query_1 .= "$k, ";
			$query_2 .= "$k = '".$_POST[$k]."' AND ";
		}
	}
	$query_1 = substr($query_1, 0, strlen($query_1)-2);
	$query_2 = substr($query_2, 0, strlen($query_2)-5);
	
	$query = "SELECT ".$query_1." FROM ".$arr['tabela']['tabela_nome']." WHERE ".$query_2;
	$res = db_query($query);

	if(is_array($res) && count($res) > 0) {
		header("Location: index.php?erro=1&campos=".$query_1); # melhorar a mensagem de erro para duplicados
		exit;
	}
	// ---------------------------------------	


	// ---------------------------------------	
	// depois de analisados os duplicados, podemos inserir na tabela o registo
	// ---------------------------------------
	// construir a query_1 e query_2, partes integrantes da instrução INSERT
	// ---------------------------------------
	$query_1 = '';
	$query_2 = '';
	
	foreach($arr_campos as $k=>$v) {
		$query_1 .= "$v, ";
		
		if($arr[$v]['tipo'] == 'file') {
			$query_2 .= "'', ";
			
		} elseif($arr[$v]['tipo'] == 'checkbox') {
			$valor = '';
			foreach($_POST[$v] as $k_checkbox=> $v_checkbox) {
				$valor .= $v_checkbox.',';
			}
			$valor = substr($valor, 0, strlen($valor)-1);
			$query_2 .= "'".$valor."', ";
			
		} elseif(isset($arr[$v]['inserir_funcao'])) {
			$param = array();
			foreach($arr[$v]['inserir_funcao']['funcao_parametros'] as $k_funcao=> $v_funcao) {
				$param[]= $_POST[$v_funcao];
			}
			$valor = call_user_func_array($arr[$v]['inserir_funcao']['funcao_nome'],$param);
			$query_2 .= "'".$valor."', ";
			
		} else {
			$query_2 .= "'".$_POST[$v]."', ";
		}
	}
	$query_1 = substr($query_1, 0, strlen($query_1)-2);
	$query_2 = substr($query_2, 0, strlen($query_2)-2);

	// ---------------------------------------	
	// no INSERT vamos incluir todos os campos assinalados como inserir => 1
	// ---------------------------------------
	$query = "INSERT INTO ".$arr['tabela']['tabela_nome']." (".$query_1.") VALUES (".$query_2.")";
	$res = db_query($query);
	$key_field = 'id='.$res;
	
	// ---------------------------------------	
	// tratar campos de imagem, vamos utilizar o ID do insert para gravar no nome da foto
	// ---------------------------------------
	$query_1 = '';
	$query_2 = '';
	$tratar_ficheiros = 0;
	
	foreach($_FILES as $k=>$v) {
		if($v['error'] == 0) {
			
			$tratar_ficheiros = 1;
			$path_parts = pathinfo($v['name']);
			if($v['size'] < $arrSETT['maxFILES_upload'] && in_array($path_parts['extension'], $arrSETT['arrFILES_upload'])) {
				$ficheiro = trata_nome_ficheiro($path_parts['filename'], $path_parts['extension'], $key_field, $arr['tabela']['tabela_nome']);
				$query_1 .= "$k = '".$ficheiro['file']."', ";
				
				move_uploaded_file($v['tmp_name'], $ficheiro['pathfile']);
			}
		}
	}
    
	if($tratar_ficheiros) {
		$query_1 = substr($query_1, 0, strlen($query_1)-2);
		foreach($arr_campos_chave as $k=>$v) {
			// ------------ TRATAR DISTO ------------
			// posso vir a ter problemas aqui, se tiver mais do que um campo chave na tabela principal
			// mas o mysqli_insert_id só devolve o valor do campo auto_increment
			
			$query_2 .= "$v = '".$res."' AND "; 
		}
		$query_2 = substr($query_2, 0, strlen($query_2)-5);
		$query = "UPDATE ".$arr['tabela']['tabela_nome']." SET ".$query_1." WHERE ".$query_2."";
		$res = db_query($query);
	}

	header("Location: index.php");
	exit;
}

function db_edit_form($arr) {
	global $arrSETT;
	echo '<div class="titulo">Editar utilizadores</div>';
	
	// ---------------------------------------	
	// determinar os campos a apresentar e qual a ordem de apresentação	
	// é obrigatório definir a variável "editar_ordem" no array
	// não pode existir dois números de ordem iguais
	// ---------------------------------------
	$arr_campos = array();
	$arr_campos_chave = array();
	foreach($arr as $k=>$v) {
		if(isset($v['editar']) && $v['editar'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos[$v['editar_ordem']] = $k;
		}
		if(isset($v['chave']) && $v['chave'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos_chave[] = $k;
		}
	}
	ksort($arr_campos);
	
	// ---------------------------------------	
	// no SELECT podemos utilizar apenas os campos que serão necessários no editar, 
	// mas vamos ter a necessidade também de incluir os campos chave
	// vai existir um filtro que são os campos do method = GET
	// ---------------------------------------
	// construir a query_1 e query_2, partes integrantes da instrução SELECT
	// ---------------------------------------
	$query_1 = '';
	$query_2 = '';
	foreach($arr_campos_chave as $k=>$v) {
		$query_1 .= "$v, ";
	}
	foreach($arr_campos as $k=>$v) {
		$query_1 .= "$v, ";
	}
	foreach($arr_campos_chave as $k=>$v) {
		$query_2 .= "$v = '".$_GET[$v]."' AND ";
	}
	$query_1 = substr($query_1, 0, strlen($query_1)-2);
	$query_2 = substr($query_2, 0, strlen($query_2)-5);
	
	$query = "SELECT ".$query_1." FROM ".$arr['tabela']['tabela_nome']." WHERE ".$query_2;
	$res = db_query($query);

	// ---------------------------------------	
	// criar o cabeçalho da tabela
	// ---------------------------------------
	echo '<table class="db_editar">';
	echo '<form name="frmEditar" method="post" action="'.$_SERVER['PHP_SELF'].'?task=do_edit" enctype="multipart/form-data">';
	foreach($arr_campos_chave as $k_arr_campos_chave=>$v_arr_campos_chave) {
		$arr[$v_arr_campos_chave]['tipo']='hidden';
		$arr[$v_arr_campos_chave]['developer_valor']=$res[0][$v_arr_campos_chave];
		echo carrega_campo($arr, $k_arr_campos_chave, $v_arr_campos_chave, 0);
	}
	foreach($arr_campos as $k_arr_campos=>$v_arr_campos) {
		$arr[$v_arr_campos]['developer_valor']=$res[0][$v_arr_campos];
		if(isset($arr[$v_arr_campos]['editar']) && $arr[$v_arr_campos]['editar'] == 1) {
			echo '<tr>';
			echo '<td class="label">'.$arr[$v_arr_campos]['label'].':</td>';
			echo '<td>';
			echo carrega_campo($arr, $k_arr_campos, $v_arr_campos, 0);
			echo '</td>';
			echo '</tr>';
		}
	}
	echo '<tr>';
	echo '<td></td>';
	echo '<td><input type="submit" name="submit" value="Editar" /></td>';
	echo '</tr>';

	// ---------------------------------------	
	// fechar a tabela
	// ---------------------------------------
	echo '</form>';
	echo '</table>';

}

function db_do_edit_form($arr) {
	global $arrSETT;
	
	// ---------------------------------------	
	// determinar os campos para edição
	// ---------------------------------------
	$arr_campos = array();
	$arr_campos_chave = array();
	foreach($arr as $k=>$v) {
		if(isset($v['editar']) && $v['editar'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos[$v['editar_ordem']] = $k;
		}
		if(isset($v['chave']) && $v['chave'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos_chave[] = $k;
		}
	}
	ksort($arr_campos);

	// ---------------------------------------	
	// construir a query_1 e query_2, partes integrantes da instrução UPDATE
	// ---------------------------------------
	$query_1 = '';
	$query_2 = '';
	$arrFILES = array();
	
	foreach($arr_campos as $k=>$v) {
		$flag_alterar_campo = 1;

		if($arr[$v]['tipo'] == 'file' && isset($arr[$v]['editar_obrigatorio']) && $arr[$v]['editar_obrigatorio'] == 0 && $_FILES[$v]['error'] == 4 && $_FILES[$v]['size'] == 0) {
			$flag_alterar_campo = 0;
		} elseif($arr[$v]['tipo'] != 'file') {
			if(isset($arr[$v]['editar_obrigatorio']) && $arr[$v]['editar_obrigatorio'] == 0 && $_POST[$v] == '') {
				$flag_alterar_campo = 0;
			}
		}
		if(isset($arr[$v]['editar_proibido']) && $arr[$v]['editar_proibido'] == 1) {
			$flag_alterar_campo = 0;
		}

		
		if($flag_alterar_campo) {
			if($arr[$v]['tipo'] == 'file') {
				// não vai fazer nada aqui
				//$query_1 .= "$v = '', ";
				
			} elseif($arr[$v]['tipo'] == 'checkbox') {
				$valor = '';
				foreach($_POST[$v] as $k_checkbox=> $v_checkbox) {
					$valor .= $v_checkbox.',';
				}
				$valor = substr($valor, 0, strlen($valor)-1);
				$query_1 .= "$v = '".$valor."', ";
			
			} elseif(isset($arr[$v]['inserir_funcao'])) {
				$param = array();
				foreach($arr[$v]['inserir_funcao']['funcao_parametros'] as $k_funcao=> $v_funcao) {
					$param[]= $_POST[$v_funcao];
				}
				$valor = call_user_func_array($arr[$v]['inserir_funcao']['funcao_nome'],$param);
				$query_1 .= "$v = '".$valor."', ";
				
			} else {
				$query_1 .= "$v = '".$_POST[$v]."', ";
			} 
		}
	}
	$key_field = '';
	foreach($arr_campos_chave as $k=>$v) {
		$key_field .= "$v=".$_POST[$v].",";
		$query_2 .= "$v = '".$_POST[$v]."' AND ";
	}
	$query_1 = substr($query_1, 0, strlen($query_1)-2);
	$query_2 = substr($query_2, 0, strlen($query_2)-5);
	$key_field = substr($key_field, 0, strlen($key_field)-1);

	// ---------------------------------------	
	// no UPDATE vamos incluir todos os campos assinalados como editar => 1
	// ---------------------------------------
	$query = "UPDATE ".$arr['tabela']['tabela_nome']." SET ".$query_1." WHERE ".$query_2."";
	$res = db_query($query);


	// ---------------------------------------	
	// tratar campos de imagem, vamos utilizar o ID do insert para gravar no nome da foto
	// ---------------------------------------
	$query_1 = '';
	$query_2 = '';
	$tratar_ficheiros = 0;
	
	foreach($_FILES as $k=>$v) {
		if($v['error'] == 0) {
			// ------------ TRATAR DISTO ------------
			// deveria fazer o unlink das fotos anteriores, para evitar lixo nas pastas

			$tratar_ficheiros = 1;
			$path_parts = pathinfo($v['name']);
			if($v['size'] < $arrSETT['maxFILES_upload'] && in_array($path_parts['extension'], $arrSETT['arrFILES_upload'])) {
				$ficheiro = trata_nome_ficheiro($path_parts['filename'], $path_parts['extension'], $key_field, $arr['tabela']['tabela_nome']);
				$query_1 .= "$k = '".$ficheiro['file']."', ";
				
				move_uploaded_file($v['tmp_name'], $ficheiro['pathfile']);
			}
		}
	}
	if($tratar_ficheiros) {
		$query_1 = substr($query_1, 0, strlen($query_1)-2);
		foreach($arr_campos_chave as $k=>$v) {
			$query_2 .= "$v = '".$_POST[$v]."' AND ";
		}
		$query_2 = substr($query_2, 0, strlen($query_2)-5);
		$query = "UPDATE ".$arr['tabela']['tabela_nome']." SET ".$query_1." WHERE ".$query_2."";
		
		$res = db_query($query);
	}

	header("Location: index.php");
	exit;
}

function db_delete_form($arr) {
	global $arrSETT;
	
	// ---------------------------------------	
	// determinar os campos colocar na regra de DELETE, todos os campos chave
	// ---------------------------------------
	$arr_campos_chave = array();
	foreach($arr as $k=>$v) {
		if(isset($v['chave']) && $v['chave'] == 1 && isset($v['campo']) && $v['campo'] == 1) {
			$arr_campos_chave[] = $k;
		}
	}

	// ---------------------------------------	
	// criar os filtros para a instrução de DELETE
	// ---------------------------------------
	$filtro = '';
	foreach($arr_campos_chave as $k=>$v) {
		$filtro .= "$v = '".$_GET[$v]."' AND ";
	}
	$filtro = substr($filtro, 0, strlen($filtro)-5);
	// ---------------------------------------	
	// no DELETE vamos eliminar os registos com a informação do(s) campo(s) chave
	// ---------------------------------------
	if (isset($_GET['tabela'])) {
        $query = "DELETE FROM ".$_GET['tabela']." WHERE ".$filtro;
        $res = db_query($query);
    }
	
	header("Location: index.php");
	exit;
}
?>