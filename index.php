<?php 
include_once 'include/settings.inc.php'; 
$page = 'Home';
include_once $arrSETT['dir_site'].'/include/topo.inc.php'; 
?>

	<!-- Featured -->
		<div id="featured">
			<div class="container">
				<header>
					<h2>Welcome to Q Framework</h2>
				</header>
                <p><strong>Q Framework</strong> focuses our resources around the world to create opportunities in the communities where we do business, fulfilling our commitment to make a positive impact on the world through innovative technologies, partnerships, giving programs and community outreach.</p>
				<hr />
				<div class="row">
					<section class="4u">
						<span class="pennant"><span class="fa fa-briefcase"></span></span>
						<h3>Our Mission</h3>
						<p>Empower every person and every organization on the planet to achieve more.</p>
						<a href="<?php echo $arrSETT['url_site']; ?>/about" class="button button-style1">Read More</a>
					</section>
					<section class="4u">
						<span class="pennant"><span class="fa fa-lock"></span></span>
						<h3>Our Strategy</h3>
						<p>Build best-in-class platforms and productivity services for a mobile-first, cloud-first world.</p>
						<a href="<?php echo $arrSETT['url_site']; ?>/about" class="button button-style1">Read More</a>
					</section>
					<section class="4u">
						<span class="pennant"><span class="fa fa-globe"></span></span>
						<h3>Our Ambitions</h3>
						<p>Reinvent productivity &amp; business processes, build the intelligent cloud platform, create more personal computing.</p>
						<a href="<?php echo $arrSETT['url_site']; ?>/about" class="button button-style1">Read More</a>
					</section>

				</div>
			</div>
		</div>

	<!-- Main -->
		<div id="main">
            <?php
            $query = "SELECT * FROM news WHERE ativo = 1 ORDER BY id DESC LIMIT 0,4";
            $res = db_query($query);
            if(is_array($res)) {
            ?>
			<div id="content" class="container">
                
				<div class="row">
                    
                    <?php
                    if(isset($res[0])) {
                    ?>
					<section class="6u">
						<a href="<?php echo $arrSETT['url_site'].'/news#'.$res[0]['id']; ?>" class="image full"><img src="<?php echo $arrSETT['url_site'] . '/upload/news/'.$res[0]['foto']; ?>" alt="<?php echo $res[0]['titulo']; ?>"></a>
						<header>
							<h2><?php echo $res[0]['titulo']; ?></h2>
						</header>
						<p><?php echo $res[0]['resumo']; ?></p>
					</section>
                    <?php
                    }
                    if(isset($res[1])) {
                    ?>
					<section class="6u">
						<a href="<?php echo $arrSETT['url_site'].'/news#'.$res[1]['id']; ?>" class="image full"><img src="<?php echo $arrSETT['url_site'] . '/upload/news/'.$res[1]['foto']; ?>" alt="<?php echo $res[1]['titulo']; ?>"></a>
						<header>
							<h2><?php echo $res[1]['titulo']; ?></h2>
						</header>
						<p><?php echo $res[1]['resumo']; ?></p>
					</section>
                    <?php
				    }
				    ?>
                </div>

				<div class="row">
                    
					<?php
                    if(isset($res[2])) {
                    ?>
					<section class="6u">
						<a href="<?php echo $arrSETT['url_site'].'/news#'.$res[2]['id']; ?>" class="image full"><img src="<?php echo $arrSETT['url_site'] . '/upload/news/'.$res[2]['foto']; ?>" alt="<?php echo $res[2]['titulo']; ?>"></a>
						<header>
							<h2><?php echo $res[2]['titulo']; ?></h2>
						</header>
						<p><?php echo $res[2]['resumo']; ?></p>
					</section>
                    <?php
                    }
                    if(isset($res[3])) {
                    ?>
					<section class="6u">
						<a href="<?php echo $arrSETT['url_site'].'/news#'.$res[3]['id']; ?>" class="image full"><img src="<?php echo $arrSETT['url_site'] . '/upload/news/'.$res[3]['foto']; ?>" alt="<?php echo $res[3]['titulo']; ?>"></a>
						<header>
							<h2><?php echo $res[3]['titulo']; ?></h2>
						</header>
						<p><?php echo $res[3]['resumo']; ?></p>
					</section>
			
            <?php } ?>
				</div>
            <?php } ?>
			</div>
		</div>

<?php 
include_once $arrSETT['dir_site'].'/include/rodape.inc.php'; 
?>