<?php
# Configurações gerais
# ---------------------------------------
$arrSETT['nome_site'] = 'Q Frame';
$arrSETT['ano_site'] = '2016';
$arrSETT['developer_site'] = 'QuarkProjects';

# Caminhos absolutos para o directório (dir_site) e url (url_site) do backoffice
# ---------------------------------------
$backoffice = 'admin';
$arrSETT['dir_site_admin'] = $arrSETT['dir_site'].'/'.$backoffice;
$arrSETT['url_site_admin'] = $arrSETT['url_site'].'/'.$backoffice;

# Caminhos absolutos para o directório  das fotografias
# ---------------------------------------
$pasta = 'upload';
$arrSETT['dir_fotos'] = $arrSETT['dir_site'].'/'.$pasta;
$arrSETT['url_fotos'] = $arrSETT['url_site'].'/'.$pasta;
$arrSETT['dir_fotos_admin'] = $arrSETT['dir_site_admin'].'/'.$pasta;
$arrSETT['url_fotos_admin'] = $arrSETT['url_site_admin'].'/'.$pasta;

# Caminhos absolutos para o directório dos icons
# ---------------------------------------
$pasta = 'icons';
$arrSETT['dir_icons'] = $arrSETT['dir_site'].'/imgs/'.$pasta;
$arrSETT['url_icons'] = $arrSETT['url_site'].'/imgs/'.$pasta;
$arrSETT['dir_icons_admin'] = $arrSETT['dir_site_admin'].'/imgs/'.$pasta;
$arrSETT['url_icons_admin'] = $arrSETT['url_site_admin'].'/imgs/'.$pasta;

# Definições de tipos de ficheiros permitidos para upload e tamanho máximo
# ---------------------------------------
$arrSETT['arrFILES_upload'] = array('gif','jpg','png');
$arrSETT['maxFILES_upload'] = 2097152; # 2Mb = 2 * 1024 (Kb) * 1024 (Bytes);
?>