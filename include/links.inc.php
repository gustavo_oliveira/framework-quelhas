<br><br>   
<div id="featured">
    <div class="container">
        <div class="row">
            <section class="4u">
                <span class="pennant"><span class="fa fa-book"></span></span>
                <h3>About Us</h3>
                <a href="<?php echo $arrSETT['url_site']; ?>/about" class="button button-style1">Read More</a>
            </section>
            <section class="4u">
                <span class="pennant"><span class="fa fa-question-circle"></span></span>
                <h3>FAQs</h3>
                <a href="<?php echo $arrSETT['url_site']; ?>/about?page=FAQs" class="button button-style1">Read More</a>
            </section>
            <section class="4u">
                <span class="pennant"><span class="fa fa-map-o"></span></span>
                <h3>Contacts</h3>
                <a href="<?php echo $arrSETT['url_site']; ?>/about?page=Contacts" class="button button-style1">Read More</a>
            </section>

        </div>
    </div>
</div>