        <!-- Tweet -->
		<div id="tweet">
			<div class="container">
				<section>
					<blockquote>&ldquo;Be a yardstick of quality. Some people aren’t used to an environment where excellence is expected.&rdquo; - Steve Jobs</blockquote>
				</section>
			</div>
		</div>
	   <!-- /Tweet -->

        <!-- Footer -->
		<div id="footer">
			<div class="container">
				<section>
					<header>
						<h2>Get in touch</h2>
						<span class="byline">Follow us in the socials!</span>
					</header>
					<ul class="contact">
						<!-- <li><a href="#" class="fa fa-twitter"><span>Twitter</span></a></li> -->
						<li><a href="https://www.facebook.com/quarkprojects/" class="fa fa-facebook" target="_blank"><span>Facebook</span></a></li>
						<!-- <li><a href="#" class="fa fa-dribbble"><span>Pinterest</span></a></li> -->
						<!-- <li><a href="#" class="fa fa-tumblr"><span>Google+</span></a></li> -->
					</ul>
				</section>
			</div>
		</div>

	<!-- Copyright -->
		<div id="copyright">
			<div class="container">
                <a href="http://quarkprojects.tk" target="_blank"><?php echo $arrSETT['developer_site']; ?></a> &copy; <?php echo $arrSETT['ano_site']; ?> - Todos os direitos reservados <br>
				Design: <a href="http://templated.co">TEMPLATED</a><br>
                Images: <a href="http://unsplash.com">Unsplash</a><br>
                <?php
                $audio = 'bgsound0'.rand(1, 5);
                switch ($audio) {
                    case 'bgsound01':
                        echo 'Orange - Piano Version';
                        break;
                    case 'bgsound02':
                        echo 'Pokémon Battle - Piano';
                        break;
                    case 'bgsound03':
                        echo 'Hikaru Nara - Piano Version';
                        break;
                    case 'bgsound04':
                        echo 'Fur Eliese - Piano';
                        break;
                    case 'bgsound05':
                        echo 'Non Non Biyori Theme - Piano';
                        break;
                }
                ?><br>
                <a href="javascript:void(0);" onclick="StopAudio();"><i class="fa fa-pause"></i> Stop / Play <i class='fa fa-play'></i></a><br>
                <a href="javascript:void(0);" onclick="AudioMin();"><i class="fa fa-volume-down"></i></a> &nbsp; <a href="javascript:void(0);" onclick="AudioPlus();"><i class='fa fa-volume-up'></i></a>
			</div>
		</div>
        <audio loop id='bgAudio'>
            <source src="<?php echo $arrSETT['url_site']."/music/$audio.mp3"?>" type="audio/mpeg">
        </audio>
	</body>
</html>
<?php db_close(); ?>