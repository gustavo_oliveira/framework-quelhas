                    <!-- Sidebar -->
					<div id="sidebar" class="4u">
                        <?php
                        $query = "SELECT * FROM news WHERE ativo = 1";
                        $res = db_query($query);
                        shuffle($res);
                        if (is_array($res)) {
                        ?>
						<section>
							<header>
								<h2>Últimas Notícias!</h2>
							</header>
							<div class="row">
								<section class="6u">
									<ul class="default">
                                        <?php
                                        if (isset($res[0])) {
                                        ?>
										<li><a href="#<?php echo $res[0]['id']; ?>"><?php echo $res[0]['titulo']; ?></a></li>
                                        <?php } ?>
                                        <?php
                                        if (isset($res[1])) {
                                        ?>
										<li><a href="#<?php echo $res[1]['id']; ?>"><?php echo $res[1]['titulo']; ?></a></li>
                                        <?php } ?>
									</ul>
								</section>
								<section class="6u">
									<ul class="default">
										<?php
                                        if (isset($res[2])) {
                                        ?>
										<li><a href="#<?php echo $res[2]['id']; ?>"><?php echo $res[2]['titulo']; ?></a></li>
                                        <?php } ?>
                                        <?php
                                        if (isset($res[3])) {
                                        ?>
										<li><a href="#<?php echo $res[3]['id']; ?>"><?php echo $res[3]['titulo']; ?></a></li>
                                        <?php } ?>
									</ul>
								</section>
							</div>
						</section>
                        <?php
                        }
                        $query = "SELECT * FROM events WHERE ativo = 1";
                        $res = db_query($query);
                        shuffle($res);
                        if(is_array($res)) {
                        ?>
						<section>
							<header>
								<h2>Latest events!</h2>
							</header>
							<ul class="style">
                                <?php
                                if(isset($res[0])) {
                                ?>
								<li>
									<p class="posted"><?php echo data_extenso(substr($res[0]['data'], 0, 4), intval(substr($res[0]['data'], 5, 2))-1, substr($res[0]['data'], 8, 2)); ?></p>
									<p><a href="#<?php echo $res[0]['id']; ?>"><?php echo $res[0]['titulo']; ?></a></p>
								</li>
                                <?php
                                }
                                if(isset($res[1])) {
                                ?>
								<li>
									<p class="posted"><?php echo data_extenso(substr($res[1]['data'], 0, 4), intval(substr($res[1]['data'], 5, 2)), substr($res[1]['data'], 8, 2)); ?></p>
									<p><a href="#<?php echo $res[1]['id']; ?>"><?php echo $res[1]['titulo']; ?></a></p>
								</li>
                                <?php
                                }
                                ?>
							</ul>
						</section>
                        <?php
                        }
                        ?>
					</div>