<!DOCTYPE HTML>
<!--
	Linear by TEMPLATED
    templated.co @templatedco
    Released for free under the Creative Commons Attribution 3.0 license (templated.co/license)
-->
<html>
	<head>
		<title><?php echo $arrSETT['nome_site'].' '.$page; ?></title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,700,500,900' rel='stylesheet' type='text/css'>
        <link href="<?php echo $arrSETT['url_site']; ?>/css/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
		<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
		<script src="<?php echo $arrSETT['url_site']; ?>/js/skel.min.js"></script>
		<script src="<?php echo $arrSETT['url_site']; ?>/js/skel-panels.min.js"></script>
        <script>var var_prefix; var_prefix='<?php echo $arrSETT['url_site']; ?>';</script>
		<script src="<?php echo $arrSETT['url_site']; ?>/js/init.js"></script>
        <script src="<?php echo $arrSETT['url_site']; ?>/js/functions.js"></script>
		<noscript>
			<link rel="stylesheet" href="<?php echo $arrSETT['url_site']; ?>/css/skel-noscript.css" />
			<link rel="stylesheet" href="<?php echo $arrSETT['url_site']; ?>/css/style.css" />
			<link rel="stylesheet" href="<?php echo $arrSETT['url_site']; ?>/css/style-desktop.css" />
		</noscript>
	</head>
	<body <?php if(isset($page)) { if($page == 'Home') { ?>class="homepage"<?php }} ?>>
        
        
        	<!-- Header -->
		<div id="header">
			<div id="nav-wrapper"> 
				<!-- Nav -->
				<nav id="nav">
					<ul>
						<li><a href="<?php echo $arrSETT['url_site']; ?>/">Homepage</a></li>
						<li><a href="<?php echo $arrSETT['url_site']; ?>/news/">News</a></li>
						<li><a href="<?php echo $arrSETT['url_site']; ?>/events">Events</a></li>
						<li><a href="<?php echo $arrSETT['url_site']; ?>/about">About</a></li>
					</ul>
				</nav>
			</div>
			<div class="container"> 
				
				<!-- Logo -->
				<div id="logo">
					<h1><a href="<?php echo $arrSETT['url_site']; ?>"><i class="fa fa-trophy"></i> <?php echo $arrSETT['nome_site']; ?></a></h1>
					<span class="tag"><i class="fa fa-diamond"></i> By Quelhas <i class="fa fa-diamond"></i></span>
				</div>
			</div>
		</div>
	<!-- Header --> 