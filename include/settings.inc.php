<?php
@session_start();
error_reporting(-1);
header('Content-Type: text/html; charset=utf-8');

$arrSETT['debug_time'] = 1;
$microtimeref = microtime(true);

# FRAMEWORK DESENVOLVIDA POR: Quelhas, Gustavo
# QuarkProjects Corporation © ALL RIGHTS RESERVED
# ANO DE DESENVOLVIMENTO: 2016

# Caminhos absolutos para o directório (dir_site) e url (url_site) do projeto
# ---------------------------------------
$arrSETT['dir_site'] = 'C:/Share/12itm11/www/QUELHAS/framework-quelhas';
$arrSETT['url_site'] = 'http://web.colgaia.local/12itm11/QUELHAS/framework-quelhas';
//$arrSETT['dir_site'] = 'D:/xampp/htdocs/framework-quelhas';
//$arrSETT['url_site'] = 'http://localhost/framework-quelhas';
# ---------------------------------------

# Definições gerais do projeto
# ---------------------------------------
include_once $arrSETT['dir_site'].'/include/settings/project.config.php'; 
# ---------------------------------------

# Definições gerais do acesso à base de dados remota (mysql)
# ---------------------------------------
include_once $arrSETT['dir_site'].'/include/settings/db.config.php'; 
include_once $arrSETT['dir_site'].'/lib/db.inc.php'; 
$arrSETT['db_link'] = db_connect();
# ---------------------------------------

# Incluir todas as librarias, tais como a libraria de funções
# ---------------------------------------
include_once $arrSETT['dir_site'].'/lib/functions.inc.php'; 
include_once $arrSETT['dir_site'].'/lib/functions.db.inc.php';
# ---------------------------------------

stats();
?>