 <?php 
include_once '../include/settings.inc.php'; 
$page = 'Events';
include_once $arrSETT['dir_site'].'/include/topo.inc.php'; 
?>



	<!-- Main -->
		<div id="main">
			<div class="container">
				<div class="row">

                    <?php
                    
                    $query = "SELECT * FROM events WHERE ativo = 1";
                    $res = db_query($query);
			        if(is_array($res)) {
                    
                    ?>
					<!-- Content -->
					<div id="content" class="8u skel-cell-important">
                        <?php 
                        for ($i = 0; $i < count($res); $i++) {
                        if(isset($res[$i])) {
                        ?>
						<section  id="<?php echo $res[$i]['id'] ?>">
							<header>
								<h2><?php 
                                echo $res[$i]['titulo'];
                                ?></h2>
								<span class="byline"><?php 
                                echo $res[$i]['resumo'];
                                ?></span>
                                <img alt="<?php echo $res[$i]['titulo']; ?>" src="<?php echo $arrSETT['url_site'].'/upload/events/'.$res[$i]['foto']; ?>" />
							</header>
							<?php 
                            echo $res[$i]['texto'];
                            ?>
						</section>
                        <?php } } ?>
					</div>
            
                    <?php 
                    }
                    ?>
					<?php
                    include $arrSETT['dir_site'].'/include/latest.inc.php';
                    ?>
					
				</div>
			</div>
		</div>
	<!-- /Main -->


<?php 
include_once $arrSETT['dir_site'].'/include/rodape.inc.php'; 
?>