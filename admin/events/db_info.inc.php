<?php
$arrForm = array(	'tabela' 	=> array('tabela' => 1, 
										 'tabela_nome' => 'events', 
										 'listagem_campos' => '*'
										 ),
					
					'inserir' 	=> array('listagem' => 1, 
										 'label' => 'INSERIR', 
										 'icon' => 'insert.png', 
										 'icon_hover' => 'insert_2.png'
										 ),
					
					'editar' 	=> array('listagem' => 1,
										 'listagem_width' => '100',
										 'label' => 'EDITAR', 
										 'icon' => 'edit.png', 
										 'icon_hover' => 'edit_2.png'
										 ),

					'eliminar' 	=> array('listagem' => 1, 
										 'listagem_width' => '100',
										 'label' => 'ELIMINAR', 
										 'icon' => 'delete.png', 
										 'icon_hover' => 'delete_2.png'
										 ),
										 
					'id' 		=> array('campo' => 1, 
										 'chave' => 1, 
										 'label' => 'ID', 
										 'listagem' => 1, 
										 'listagem_ordem' => 1,
										 'listagem_width' => '20',
										 'inserir' => 0, 
										 'inserir_ordem' => 1,
										 'editar' => 0, 
										 'editar_ordem' => 1
										 ),
										 
					'titulo' 		=> array('campo' => 1,
										 'tipo' => 'text', 
										 'tamanho' => 255, 
										 'label' => 'TITULO', 
										 'listagem' => 1, 
										 'listagem_ordem' => 2,
										 'listagem_width' => '200',
										 'inserir' => 1, 
										 'inserir_ordem' => 2,
										 'editar' => 1, 
										 'editar_ordem' => 2,
										 'lingua' => 1
										 ),
										 
					'resumo' 		=> array('campo' => 1, 
										 'tipo' => 'textarea', 
										 'tamanho' => 255, 
										 'label' => 'RESUMO', 
										 'listagem' => 1, 
										 'listagem_ordem' => 3,
										 'listagem_width' => '100',
										 'inserir' => 1, 
										 'inserir_ordem' => 3,
										 'editar' => 1, 
										 'editar_ordem' => 3
										 ),
										 
					'texto' 	=> array('campo' => 1, 
										 'tipo' => 'ckeditor', 
										 'tamanho' => 2024, 
										 'label' => 'TEXTO', 
										 'listagem' => 0, 
										 'listagem_ordem' => 4,
										 'inserir' => 1, 
										 'inserir_ordem' => 4,
										 'editar' => 1, 
										 'editar_ordem' => 4
										 ),
										 
					'data' 		=> array('campo' => 1, 
										 'tipo' => 'data', 
										 'label' => 'DATA', 
										 'listagem' => 0, 
										 'listagem_ordem' => 5,
										 'listagem_width' => '50',
										 'inserir' => 1, 
										 'inserir_ordem' => 5,
										 'editar' => 1, 
										 'editar_ordem' => 5
										 ),
				 
					'foto' 		=> array('campo' => 1, 
										 'tipo' => 'file', 
										 'label' => 'FOTO', 
										 'listagem' => 1, 
										 'listagem_ordem' => 6,
										 'listagem_width' => '50',
										 'inserir' => 1, 
										 'inserir_ordem' => 6,
										 'editar' => 1, 
										 'editar_ordem' => 6,
										 'editar_obrigatorio' => 0
										 ),
					
					'ativo' 	=> array('campo' => 1, 
										 'tipo' => 'checkbox', 
										 'default' => 1, 
										 'opcoes' => array(1 => 'Yes, the event is available at the website'), 
										 'label' => 'ATIVO', 
										 'listagem' => 1, 
										 'listagem_ordem' => 9,
										 'listagem_width' => '50',
										 'inserir' => 1, 
										 'inserir_ordem' => 9,
										 'editar' => 1, 
										 'editar_ordem' => 9
										 )
				);
?>