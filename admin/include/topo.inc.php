<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo $arrSETT['nome_site']; ?> Admin Zone</title>
<link href="<?php echo $arrSETT['url_site_admin']; ?>/css/framework.css" rel="stylesheet" type="text/css" />

<link href="<?php echo $arrSETT['url_site_admin']; ?>/css/font-awesome-4.5.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

<link rel="shortcut icon" href="<?php echo $arrSETT['url_site_admin']; ?>/imgs/favicon.ico">
<!-- Design by Freepik -->

<script src="<?php echo $arrSETT['url_site']; ?>/js/ckeditor.js"></script>
</head>
<body>
<?php include_once $arrSETT['dir_site_admin'].'/include/menu.inc.php'; ?>