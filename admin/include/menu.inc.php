<?php
if (isset($_SESSION['ADM_LOGIN'])) { ?>

<br>
<a id='logout' href='<?php echo $arrSETT['url_site']; ?>/admin/logout.php'>Logout <i class='fa fa-power-off'></i></a>
<a href='<?php echo $arrSETT['url_site']; ?>/admin/'><i class='fa fa-home'></i> Home Page</a>

<a href='<?php echo $arrSETT['url_site']; ?>/admin/users/'><i class='fa fa-group'></i> User Management</a>

<a href='<?php echo $arrSETT['url_site']; ?>/admin/news/'><i class='fa fa-newspaper-o'></i> News Management</a>

<a href='<?php echo $arrSETT['url_site']; ?>/admin/events/'><i class='fa fa-feed'></i> Events Management</a>

<a href='<?php echo $arrSETT['url_site']; ?>/admin/stats/'><i class='fa fa-pie-chart'></i> Statistics</a>

<span id='backoffice'>
<p>Backoffice - <?php echo $_SESSION['ADM_NOME']; ?></p>
</span>
<br><br>

<?php
}
?>
