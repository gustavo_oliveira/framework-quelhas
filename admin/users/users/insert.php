<?php 
include_once '../../include/settings.inc.php'; 
include_once $arrSETT['dir_site_admin'].'/include/control.inc.php'; 
include_once $arrSETT['dir_site_admin'].'/include/topo.inc.php'; 
include_once 'db_info.inc.php'; 
?>
<div id="content">
    
	<h1>Table Insert</h1>
	
	<div id="usrdata">
	<?php 
    if (!isset($_GET['tabela'])) {
        header('Location: '.$arrSETT['dir_site_admin'].'/users/index.php');
        die();
    } else if ($_GET['tabela'] == 'adminusers') {
        echo '<h2>Admins Table</h2>';
        $arr = $arrAdmTab;
    } else if ($_GET['tabela'] == 'users') {
        echo '<h2>Users Table</h2>';
        $arr = $arrUsrTab;
    }
    
//    pr($arr);
//    die;
    
    db_insert_form($arr);
    ?>
        
	<br>
	
</div>

<a id="logout" href="<?php echo $arrSETT['url_site_admin'] ?>/users/index.php">Back</a>

<?php 
include_once $arrSETT['dir_site_admin'].'/include/rodape.inc.php'; 
?>