<?php
$arrAdmTab = array(	'tabela' 	=> array('tabela' => 1, 
										 'tabela_nome' => 'adminusers', 
										 'listagem_campos' => '*'
										 ),
					
					'inserir' 	=> array('listagem' => 1, 
										 'label' => 'INSERT', 
										 'icon' => 'insert.png', 
										 'icon_hover' => 'insert_2.png'
										 ),
					
					'editar' 	=> array('listagem' => 1,
										 'listagem_width' => '100',
										 'label' => 'EDIT', 
										 'icon' => 'edit.png', 
										 'icon_hover' => 'edit_2.png'
										 ),

					'eliminar' 	=> array('listagem' => 1, 
										 'listagem_width' => '100',
										 'label' => 'DELETE', 
										 'icon' => 'delete.png', 
										 'icon_hover' => 'delete_2.png'
										 ),
										 
					'id' 		=> array('campo' => 1, 
										 'chave' => 1, 
										 'label' => 'ID', 
										 'listagem' => 1, 
										 'listagem_ordem' => 1,
										 'listagem_width' => '20',
										 'inserir' => 0, 
										 'inserir_ordem' => 1,
										 'editar' => 0, 
										 'editar_ordem' => 1
										 ),
										 
					'nome' 		=> array('campo' => 1,
										 'tipo' => 'text', 
										 'tamanho' => 100, 
										 'label' => 'NAME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 3,
										 'listagem_width' => '200',
										 'inserir' => 1, 
										 'inserir_ordem' => 2,
										 'editar' => 1, 
										 'editar_ordem' => 2,
										 'lingua' => 1
										 ),
										 
					'username' 	=> array('campo' => 1, 
										 'tipo' => 'text', 
										 'tamanho' => 20, 
										 'label' => 'USERNAME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 2,
										 'listagem_width' => '100',
										 'inserir' => 1, 
										 'inserir_ordem' => 3,
										 'editar' => 1, 
										 'editar_ordem' => 3,
										 'editar_proibido' => 1
										 ),
										 
					'password' 	=> array('campo' => 1, 
										 'tipo' => 'password', 
										 'tamanho' => 50, 
										 'label' => 'PASSWORD', 
										 'listagem' => 0, 
										 'listagem_ordem' => 4,
										 'inserir' => 1, 
										 'inserir_ordem' => 4,
										 'editar' => 1, 
										 'editar_ordem' => 4,
										 'editar_obrigatorio' => 0,
										 'inserir_funcao' => array('funcao_nome' => 'passGen', 
																   'funcao_parametros' => array('username','password')
																   )
										 ),
										 /*
					'foto' 		=> array('campo' => 1, 
										 'tipo' => 'file', 
										 'label' => 'IMAGEM', 
										 'listagem' => 0, 
										 'listagem_ordem' => 6,
										 'listagem_width' => '50',
										 'inserir' => 1, 
										 'inserir_ordem' => 6,
										 'editar' => 1, 
										 'editar_ordem' => 6
										 ),
										 */
					'ativo' 	=> array('campo' => 1, 
										 'tipo' => 'checkbox', 
										 'default' => 1, 
										 'opcoes' => array(1 => 'Yep! User is authorized to login.'), 
										 'label' => 'ACTIVE', 
										 'listagem' => 1, 
										 'listagem_ordem' => 7,
										 'listagem_width' => '50',
										 'inserir' => 1, 
										 'inserir_ordem' => 7,
										 'editar' => 1, 
										 'editar_ordem' => 7
										 )
				);

$arrUsrTab = array(	'tabela' 	=> array('tabela' => 1, 
										 'tabela_nome' => 'users', 
										 'listagem_campos' => '*'
										 ),
					
					'inserir' 	=> array('listagem' => 1, 
										 'label' => 'INSERT', 
										 'icon' => 'insert.png', 
										 'icon_hover' => 'insert_2.png'
										 ),
					
					'editar' 	=> array('listagem' => 1,
										 'listagem_width' => '100',
										 'label' => 'EDIT', 
										 'icon' => 'edit.png', 
										 'icon_hover' => 'edit_2.png'
										 ),

					'eliminar' 	=> array('listagem' => 1, 
										 'listagem_width' => '100',
										 'label' => 'DELETE', 
										 'icon' => 'delete.png', 
										 'icon_hover' => 'delete_2.png'
										 ),
										 
					'id' 		=> array('campo' => 1, 
										 'chave' => 1, 
										 'label' => 'ID', 
										 'listagem' => 1, 
										 'listagem_ordem' => 1,
										 'listagem_width' => '20',
										 'inserir' => 0, 
										 'inserir_ordem' => 1,
										 'editar' => 0, 
										 'editar_ordem' => 1
										 ),
										 
					'nome' 		=> array('campo' => 1,
										 'tipo' => 'text', 
										 'tamanho' => 100, 
										 'label' => 'NAME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 3,
										 'listagem_width' => '200',
										 'inserir' => 1, 
										 'inserir_ordem' => 2,
										 'editar' => 1, 
										 'editar_ordem' => 2,
										 'lingua' => 1
										 ),
										 
					'username' 	=> array('campo' => 1, 
										 'tipo' => 'text', 
										 'tamanho' => 20, 
										 'label' => 'USERNAME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 2,
										 'listagem_width' => '100',
										 'inserir' => 1, 
										 'inserir_ordem' => 3,
										 'editar' => 1, 
										 'editar_ordem' => 3,
										 'editar_proibido' => 1
										 ),
										 
					'password' 	=> array('campo' => 1, 
										 'tipo' => 'password', 
										 'tamanho' => 50, 
										 'label' => 'PASSWORD', 
										 'listagem' => 0, 
										 'listagem_ordem' => 4,
										 'inserir' => 1, 
										 'inserir_ordem' => 4,
										 'editar' => 1, 
										 'editar_ordem' => 4,
										 'editar_obrigatorio' => 0,
										 'inserir_funcao' => array('funcao_nome' => 'passGen', 
																   'funcao_parametros' => array('username','password')
																   )
										 ),
										 /*
					'foto' 		=> array('campo' => 1, 
										 'tipo' => 'file', 
										 'label' => 'IMAGEM', 
										 'listagem' => 0, 
										 'listagem_ordem' => 6,
										 'listagem_width' => '50',
										 'inserir' => 1, 
										 'inserir_ordem' => 6,
										 'editar' => 1, 
										 'editar_ordem' => 6
										 ),
										 */
					'activo' 	=> array('campo' => 1, 
										 'tipo' => 'checkbox', 
										 'default' => 1, 
										 'opcoes' => array(1 => 'Sim o utilizador está autorizado a entrar no sistema'), 
										 'label' => 'ACTIVE', 
										 'listagem' => 1, 
										 'listagem_ordem' => 7,
										 'listagem_width' => '50',
										 'inserir' => 1, 
										 'inserir_ordem' => 7,
										 'editar' => 1, 
										 'editar_ordem' => 7
										 )
				);
?>