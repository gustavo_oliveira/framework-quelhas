<?php
$arrAdmTab = array(	'tabela' 	=> array('tabela' => 1, 
										 'tabela_nome' => 'adminusers', 
										 'listagem_campos' => '*'
                                        ),
										 
                   'inserir' 	=> array('listagem' => 1, 
										 'label' => 'INSERT', 
										 'icon' => 'insert.png',
                                         'icon_hover' => 'insert_2.png'
										 ),
					
					'editar' 	=> array('listagem' => 1, 
										 'label' => 'EDIT', 
										 'icon' => 'edit.png',
                                         'icon_hover' => 'edit_2.png'
										 ),

					'eliminar' 	=> array('listagem' => 1, 
										 'label' => 'DELETE', 
										 'icon' => 'delete.png', 
                                         'icon_hover' => 'delete_2.png'
										 ),
										 
					'id' 		=> array('campo' => 1, 
										 'chave' => 1, 
										 'label' => 'ID', 
										 'listagem' => 1, 
										 'listagem_ordem' => 1),
										 
					'nome' 		=> array('campo' => 1, 
										 'tipo' => 'text', 
										 'tamanho' => 100, 
										 'label' => 'NOME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 2,
                                         'inserir' => 1, 
										 'inserir_ordem' => 1),
										 
					'username' 	=> array('campo' => 1, 
										 'tipo' => 'text', 
										 'tamanho' => 20, 
										 'label' => 'USERNAME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 3,
                                         'inserir' => 1, 
										 'inserir_ordem' => 2),
										 
					'password' 	=> array('campo' => 1, 
										 'tipo' => 'password', 
										 'tamanho' => 50, 
										 'label' => 'PASSWORD', 
										 'listagem' => 0, 
										 'listagem_ordem' => 4,
                                         'inserir' => 1, 
										 'inserir_ordem' => 3,
										 'inserir_funcao' => array('funcao_nome' => 'passGen', 
																   'funcao_parametros' => array('username','password')
																   )
                                        ),
										 
					'nivel' 	=> array('campo' => 1, 
										 'tipo' => 'radio', 
										 'default' => 1, 
										 'opcoes' => array(1 => 'Utilizador', 5 => 'Administrador'), 
										 'label' => 'NIVEL', 
										 'listagem' => 0, 
										 'listagem_ordem' => 5,
                                         'inserir' => 0, 
										 'inserir_ordem' => 4),
										 
					//'foto' 	=> array(),
					'ativo' 	=> array('campo' => 1, 
										 'tipo' => 'checkbox', 
										 'default' => 1, 
										 'opcoes' => array(1 => 'Sim o utilizador está autorizado a entrar no sistema'), 
										 'label' => 'ATIVO', 
										 'listagem' => 1, 
										 'listagem_ordem' => 6,
                                         'inserir' => 1, 
										 'inserir_ordem' => 5)
				);
				
$arrUsrTab = array(	'tabela' 	=> array('tabela' => 1, 
										 'tabela_nome' => 'users', 
										 'listagem_campos' => '*'),
                   
					'inserir' 	=> array('listagem' => 0, 
										 'label' => 'INSERT', 
										 'icon' => 'insert.png', 
										 ),
					
					'editar' 	=> array('listagem' => 1, 
										 'label' => 'EDIT', 
										 'icon' => 'edit.png', 
										 ),

					'eliminar' 	=> array('listagem' => 1, 
										 'label' => 'DELETE', 
										 'icon' => 'delete.png', 
										 ),
										 
					'id' 		=> array('campo' => 1, 
										 'chave' => 1, 
										 'label' => 'ID', 
										 'listagem' => 1, 
										 'listagem_ordem' => 1),
										 
					'nome' 		=> array('campo' => 1, 
										 'tipo' => 'text', 
										 'tamanho' => 100, 
										 'label' => 'NOME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 2),
										 
					'username' 	=> array('campo' => 1, 
										 'tipo' => 'text', 
										 'tamanho' => 20, 
										 'label' => 'USERNAME', 
										 'listagem' => 1, 
										 'listagem_ordem' => 3),
										 
					'password' 	=> array('campo' => 1, 
										 'tipo' => 'password', 
										 'tamanho' => 50, 
										 'label' => 'PASSWORD', 
										 'listagem' => 0, 
										 'listagem_ordem' => 4,
										 'inserir_funcao' => array('funcao_nome' => 'passGen', 
																   'funcao_parametros' => array('username','password')
																   )
                                        ),
										 
					'nivel' 	=> array('campo' => 1, 
										 'tipo' => 'radio', 
										 'default' => 1, 
										 'label' => 'NIVEL', 
										 'listagem' => 0, 
										 'listagem_ordem' => 5),
										 
					//'foto' 	=> array(),
					'ativo' 	=> array('campo' => 1, 
										 'tipo' => 'checkbox', 
										 'default' => 1, 
										 'label' => 'ATIVO', 
										 'listagem' => 1, 
										 'listagem_ordem' => 6)
				);
?>