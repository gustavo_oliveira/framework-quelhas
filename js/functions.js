window.onload = function () {
    var backgroundAudio = document.getElementById("bgAudio");
    backgroundAudio.volume = 0.3;
    backgroundAudio.play();
}

function StopAudio () {
    var backgroundAudio = document.getElementById("bgAudio");
    if (!backgroundAudio.paused) {
        backgroundAudio.pause();
        // backgroundAudio.currentTime = 0;
    } else {
        backgroundAudio.play();
    }
}

function AudioPlus () {
    var backgroundAudio = document.getElementById("bgAudio");
    var vol = backgroundAudio.volume;
    if (vol != 1.0) {
        vol += 0.1;
        backgroundAudio.volume = vol;
    }
}

function AudioMin () {
    var backgroundAudio = document.getElementById("bgAudio");
    var vol = backgroundAudio.volume;
    if (vol != 0.0) {
        vol -= 0.1;
        backgroundAudio.volume = vol;
    }
}