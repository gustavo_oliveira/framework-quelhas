 <?php 
include_once '../include/settings.inc.php';
if (!isset($_GET['page'])) {
    $page = 'About';
} else {
    $page = $_GET['page'];
}
include_once $arrSETT['dir_site'].'/include/topo.inc.php';

//This is the CMS File
?>

	<!-- Main -->
		<div id="main">
			<div id="content" class="container">
                <?php
                if (!isset($_GET['page'])) {
                ?>
				<section>
					<header>
						<h2>About Us</h2>
						<span class="byline"><?php echo $arrSETT['nome_site']; ?>: Through the Years</span>
					</header>
					<p>The Portuguese information technology industry has grown enormously over the last few decades and the country is now a dominant player in the global market. <?php echo $arrSETT['nome_site']; ?> has long been at the forefront of this growth and although the company started life as a humble website developer with a handful of employees, it is now the leading technology company in Portugal and employs over 11,000 people around the world. <?php echo $arrSETT['nome_site']; ?> makes products in almost every conceivable category of information technology too, including PC components and peripherals, notebooks, tablets, servers and smartphones.</p>
                    
					<p>Innovation is key to the success of <?php echo $arrSETT['nome_site']; ?>.</p>
                    
					<p>We’re here to help, whether it’s for your home or business. Find various ways you can contact us.</p>							
                    
				</section>
                <?php
                } else
                if ($_GET['page'] == 'FAQs') {
                ?>
                <section>
					<header>
						<h2>FAQs</h2>
						<span class="byline">Frequently Asked Questions</span>
					</header>
					<p><b>How can you drop a raw egg onto a concrete floor without cracking it?</b></p>
                    
					<p>Concrete floors are very hard to crack!</p>
                    <br>
					<p><b>Approximately how many birthdays does the average Japanese woman have?</b></p>
                    
                    <p>Just one. All the others are anniversaries.</p>
                    <br>
					<p><b>How can you lift an elephant with one hand?</b></p>
                    
                    <p>It is not a problem, since you will never find an elephant with one hand.</p>
                    <br>
					<p><b>How can a man go eight days without sleep?</b></p>
                    
                    <p>He sleeps at night.</p>
				</section>
                <?php
                }  else
                if ($_GET['page'] == 'Contacts') {
                ?>
                <section>
					<header>
						<h2>Contacts</h2>
						<span class="byline">Any questions, don't hesitate to ask us!</span>
					</header>
					<p><b>E-mail:</b></p>
                    
					<p><a href='mailto:web.quarkprojects@gmail.com'>web.quarkprojects@gmail.com</a></p>
                    <br>
					<p><b>Address</b></p>
                    
                    <p>1015 Manhattan Avenue, Brooklyn, New York, United States</p>
                    
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.2949879769503!2d-73.95731528454712!3d40.73353414423463!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2593f18ad6b09%3A0x138f09d1e6c163fc!2s1015+Manhattan+Ave%2C+Brooklyn%2C+NY+11222%2C+USA!5e0!3m2!1spt-PT!2spt!4v1458236155739" width="800" height="600" frameborder="0" style="border:0" allowfullscreen></iframe>
				</section>
                <?php 
                }
                ?>
			</div>
            <?php
            include $arrSETT['dir_site'].'/include/links.inc.php';
            ?>
		</div>
	<!-- /Main -->

<?php 
include_once $arrSETT['dir_site'].'/include/rodape.inc.php'; 
?>